package com.example.hp.control.fragment;

public class Constants {
    public static class URL{
        private static final String HOST = "http://192.168.0.101:8080/";

        public static final String GET_REMINDERS = HOST + "reminders";
    }
}
